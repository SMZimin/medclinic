﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using App.Models;
using HttpRequests;
using DataTransferObjects;
using Records.Models;
using Communication.Models;


namespace App.Controllers
{
    public class AppController : Controller
    {
        public static string UsersManagerUrl = "http://localhost:55577";
        public static string AppUrl = "http://localhost:54055/app";
        public static string RecordsUrl = "http://localhost:54090/";
        public static string CommunicationUrl = "http://localhost:54101/";

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Prices()
        {         
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        public IActionResult RegisterUser(string email, string passwordHash, string name, string lastName,
            string middleName, string mobilePhone, DateTime birthDate, string hostName)
        {
            var user = new UserFullInfo(email, passwordHash, name, lastName, middleName, mobilePhone, birthDate);
            var response = PostRequest.ExecuteAsync<UserBasicInfo>(UsersManagerUrl + "/api/Users/RegisterUser", user).Result;

            if (response.IsSuccess)
                return Ok(new { uri = hostName + $"/App/RegistrationSuccess?name={name}&lastName={lastName}&middleName={middleName}" });
            else
                return Ok(new { uri = hostName + "/App/RegistrationFail" });
        }

        public IActionResult RegistrationSuccess(string name, string lastName, string middleName)
        {
            ViewData["UserFio"] = lastName + " " + name + " " + middleName;
            ViewData["AppUrl"] = AppUrl;
            return View();
        }

        public IActionResult RegistrationFail()
        {
            ViewData["RegUrl"] = AppUrl + "/Registration";
            return View();
        }

        [HttpPost]
        public IActionResult AuthorizeUser(string email, string passwordHash, string hostName)
        {
            var user = new UserAuthInfo(email, passwordHash);
            var response = PostRequest.ExecuteAsync<UserAuthResponse>(UsersManagerUrl + "/api/Users/VerifyUser", user).Result;
            
            if (response.IsSuccess)
            {
                TempData["LayoutFio"] = response.Data.UserBasicInfo.LastName + " " + response.Data.UserBasicInfo.Name;
                TempData["TokenBody"] = response.Data.TokenBody;

                return Ok(new { uri = hostName });
            }
            else
                return Ok(new { uri = hostName + "/App/Authorize?fail=1" });
        }

        public IActionResult Authorize(int fail = 0)
        {
            ViewData["FailFlg"] = fail;
            return View();
        }

        public RedirectResult Exit()
        {
            TempData.Remove("LayoutFio");
            TempData.Remove("TokenBody");
            
            return Redirect(AppUrl);
        }

        [HttpGet]
        public IActionResult Doctors()
        {
            var response = GetRequest.ExecuteAsync<List<Doctor>>(RecordsUrl + "/api/Records/Doctors").Result;

            if (response.IsSuccess)
            {
                return View(response.Data);
            }
            else
            {
                ViewData["ErrorTextMessage"] = "Невозможно получить информацию о докторах.";
                return View("UserErrorPage");
            }
            // string speciality = Converters.SpecialityToStringConverter.Convert(doctor.Speciality);
            //string day = Converters.DayToStringConverter.Convert(doctor.Schedule);
        }

        public IActionResult CalendarReservation(string name, string lastName, string middleName)
        {
            if (TempData.ContainsKey("TokenBody"))
            {
                var responseCheckToken = PostRequest.ExecuteAsync<UserBasicInfo>(UsersManagerUrl + "/api/Users/CheckToken", TempData.Peek("TokenBody").ToString()).Result;

                if (responseCheckToken.IsSuccess)
                {
                    ViewData["UserFio"] = responseCheckToken.Data.Name + " " + responseCheckToken.Data.LastName + " " + responseCheckToken.Data.MiddleName;
                    var response = GetRequest.ExecuteAsync<Doctor>(RecordsUrl + $"/api/Records/Doctors/{name}/{lastName}/{middleName}").Result;
                    if (response.IsSuccess)
                    {
                        return View(response.Data);
                    }
                    else
                    {
                        ViewData["ErrorTextMessage"] = "Невозможно получить информацию о расписании доктора.";
                        return View("UserErrorPage");
                    }
                }
                else
                {
                    TempData.Remove("TokenBody");
                    TempData.Remove("LayoutFio");
                    return RedirectToAction("Authorize");
                }
            }
            else
            {
                return RedirectToAction("Authorize");
            } 
        }

        public IActionResult DoctorSchedule(string currentDate, string doctorId)
        {
            var response = GetRequest.ExecuteAsync<List<(DateTime,bool)>>(RecordsUrl + $"/api/Records/Doctors/{currentDate}/{doctorId}").Result;

            if (response.IsSuccess)
            {      
                ViewData["doctorId"] = doctorId;
                return View(response.Data);
            }
            else
            {
                ViewData["ErrorTextMessage"] = "Невозможно получить информацию о рабочем графике и занятости доктора.";
                return View("UserErrorPage");
            }
            // string speciality = Converters.SpecialityToStringConverter.Convert(doctor.Speciality);
            //string day = Converters.DayToStringConverter.Convert(doctor.Schedule);
        }
        
        public IActionResult AddUserReservation(string time, string doctorId)
        {
            if (!TempData.ContainsKey("TokenBody"))
            {
                ViewData["ErrorTextMessage"] = "Невозможно получить информацию о токене";
                return View("UserErrorPage");
            }

            var responseCheckToken = PostRequest.ExecuteAsync<UserBasicInfo>(UsersManagerUrl + "/api/Users/CheckToken", TempData.Peek("TokenBody").ToString()).Result;
            if (responseCheckToken.IsSuccess)
            {
                ReservationInfo reservationInfo = new ReservationInfo(responseCheckToken.Data.Id, Guid.Parse(doctorId), DateTime.Parse(time));
                var response = PostRequest.ExecuteAsync<(Reservation,bool)>(RecordsUrl + "/api/Records/AddReservation", reservationInfo).Result;
                if (!response.IsSuccess)
                {
                    ViewData["ErrorTextMessage"] = "Ошибка сервера. Попробуйте позже.";
                    return View("UserErrorPage");
                }

                if (response.Data.Item2 == false)
                {
                    if (response.Data.Item1 != null)
                        return View(response.Data.Item1);
                    else
                    {
                        ViewData["ErrorTextMessage"] = "Невозможно добавить информацию о записе ко врачу. У вас уже есть активная запись. Обратитесь к администратору.";
                        return View("UserErrorPage");
                    }
                }
                else
                {
                    ViewData["ErrorTextMessage"] = "Невозможно добавить информацию о записе ко врачу. Ошибка сервера. Доктор не найден";
                    return View("UserErrorPage");
                }
            }
            else
            {
                TempData.Remove("TokenBody");
                TempData.Remove("LayoutFio");
                return RedirectToAction("Authorize");
            }
        }

        public IActionResult UserReservation()
        {
            if (!TempData.ContainsKey("TokenBody"))
            {
                ViewData["ErrorTextMessage"] = "Невозможно получить информацию о токене";
                return View("UserErrorPage");
            }

            var responseCheckToken = PostRequest.ExecuteAsync<UserBasicInfo>(UsersManagerUrl + "/api/Users/CheckToken", TempData.Peek("TokenBody").ToString()).Result;
            if (responseCheckToken.IsSuccess)
            {
                if (responseCheckToken.Data.IsAdmin)
                {
                    return RedirectToAction("AdminReservationPage");
                }

                var response = PostRequest.ExecuteAsync<(Reservation, bool)>(RecordsUrl + "/api/Records/UserReservation", responseCheckToken.Data.Id).Result;
                if (!response.IsSuccess)
                {
                    ViewData["ErrorTextMessage"] = "Ошибка сервера. Попробуйте позже.";
                    return View("UserErrorPage");
                }

                if (response.Data.Item2 == false)
                {
                    if (response.Data.Item1 != null)
                        return View(response.Data.Item1);
                    else
                    {
                        ViewData["ErrorTextMessage"] = "Ошибка сервера. Попробуйте позже.";
                        return View("UserErrorPage");
                    }
                }
                else
                {
                    ViewData["ErrorTextMessage"] = "Невозможно просмотреть информацию о записе ко врачу. Ошибка сервера. Актуальные записи пользователя не найдены.";
                    return View("UserErrorPage");
                }
            }
            else
            {
                TempData.Remove("TokenBody");
                TempData.Remove("LayoutFio");
                return RedirectToAction("Authorize");
            }
        }

        public IActionResult Messages()
        {
            if (!TempData.ContainsKey("TokenBody"))
            {
                ViewData["ErrorTextMessage"] = "Невозможно получить информацию о токене";
                return View("UserErrorPage");
            }

            var responseCheckToken = PostRequest.ExecuteAsync<UserBasicInfo>(UsersManagerUrl + "/api/Users/CheckToken", TempData.Peek("TokenBody").ToString()).Result;
            if (responseCheckToken.IsSuccess)
            {
                if (responseCheckToken.Data.IsAdmin)
                    return RedirectToAction("AdminDialogs");

                var response = GetRequest.ExecuteAsync<(List<(Message, bool)>, bool)>(CommunicationUrl +  $"/api/Communication/Message/{responseCheckToken.Data.Id}").Result;
                if (!response.IsSuccess)
                {
                    ViewData["ErrorTextMessage"] = "Ошибка сервера. Попробуйте позже.";
                    return View("UserErrorPage");
                }

                if (response.Data.Item2 == false)
                {
                    return View(response.Data.Item1);
                }
                else
                {
                    ViewData["ErrorTextMessage"] = "Невозможно просмотреть информацию о ваших сообщениях. Ошибка сервера.";
                    return View("UserErrorPage");
                }
            }
            else
            {
                TempData.Remove("TokenBody");
                TempData.Remove("LayoutFio");
                return RedirectToAction("Authorize");
            }
        }

        [HttpPost]
        public IActionResult AddMessage(string messageText, string hostName)
        {
            if (TempData.ContainsKey("TokenBody"))
            {
                var responseCheckToken = PostRequest.ExecuteAsync<UserBasicInfo>(UsersManagerUrl + "/api/Users/CheckToken", TempData.Peek("TokenBody").ToString()).Result;

                if (responseCheckToken.IsSuccess)
                {
                    var responseFindAdmin = GetRequest.ExecuteAsync<UserBasicInfo>(UsersManagerUrl + "/api/Users/FindAdmin").Result;

                    if (!responseFindAdmin.IsSuccess)
                    {
                        return NotFound(new { error = "Невозможно получить информацию о получателе сообщения. Ошибка сервера" });
                    }

                    Message message = new Message(responseCheckToken.Data.Id, responseFindAdmin.Data.Id, messageText);
                    var response = PostRequest.ExecuteAsync<Message>(CommunicationUrl + "/api/Communication/AddMessage", message).Result;

                    if (response.IsSuccess)
                    {
                        return Ok(new { uri = hostName + "/App/MessageAddSuccess" });
                    }
                    else
                    {
                        return Ok(new { uri = hostName + "/App/MessageAddFail" });
                    }
                }
                else
                {
                    TempData.Remove("TokenBody");
                    TempData.Remove("LayoutFio");
                    return NotFound(new { error = "Ошибка авторизации." });
                }
            }
            else
            {
                return NotFound(new { error = "Ошибка авторизации." });
            }
        }

        public IActionResult MessageAddSuccess()
        {
            ViewData["MessagesUrl"] = AppUrl + "/Messages";
            return View();
        }

        public IActionResult MessageAddFail()
        {
            ViewData["MessagesUrl"] = AppUrl + "/Messages";
            return View();
        }


        public IActionResult AdminReservationPage()
        {
            if (!TempData.ContainsKey("TokenBody"))
            {
                ViewData["ErrorTextMessage"] = "Невозможно получить информацию о токене";
                return View("UserErrorPage");
            }

            var responseCheckToken = PostRequest.ExecuteAsync<UserBasicInfo>(UsersManagerUrl + "/api/Users/CheckToken", TempData.Peek("TokenBody").ToString()).Result;
            if (!responseCheckToken.IsSuccess)
            {
                TempData.Remove("TokenBody");
                TempData.Remove("LayoutFio");
                return RedirectToAction("Authorize");
            }

            if (!responseCheckToken.Data.IsAdmin)
            {
                ViewData["ErrorTextMessage"] = "Невозможно подтвердить права администратора. Ошибка доступа.";
                return View("UserErrorPage");
            }
            
            var responseGetUsers = GetRequest.ExecuteAsync<List<UserBasicInfo>>(UsersManagerUrl + "/api/Users/ListUsers").Result;
            if (responseGetUsers.IsSuccess)
            {
                return View(responseGetUsers.Data);
            }
            else
            {
                ViewData["ErrorTextMessage"] = "Невозможно получить список пользователей. Ошибка сервера";
                return View("UserErrorPage");
            }
        }

        public IActionResult UserReservationAdmin(string userId)
        {
            if (!TempData.ContainsKey("TokenBody"))
            {
                ViewData["ErrorTextMessage"] = "Невозможно получить информацию о токене";
                return View("UserErrorPage");
            }

            var responseCheckToken = PostRequest.ExecuteAsync<UserBasicInfo>(UsersManagerUrl + "/api/Users/CheckToken", TempData.Peek("TokenBody").ToString()).Result;
            if (responseCheckToken.IsSuccess)
            {
                if (responseCheckToken.Data.IsAdmin)
                {
                    var responseReservation = PostRequest.ExecuteAsync<(Reservation,bool)>(RecordsUrl + "/api/Records/UserReservation", Guid.Parse(userId)).Result;
                    if (!responseReservation.IsSuccess)
                    {
                        ViewData["ErrorTextMessage"] = "Невозможно получить список записей пользователя. Ошибка сервера";
                        return View("UserErrorPage");
                    }
                    
                    if (responseReservation.Data.Item2 == true)
                    {
                        
                        ViewData["ErrorTextMessage"] = "Невозможно получить список записей пользователя. У пользователя отсутствуют записи";
                        return View("UserErrorPage");
                    }

                    return View(responseReservation.Data.Item1);
                }
                else
                {
                    ViewData["ErrorTextMessage"] = "Невозможно подтвердить права администратора. Ошибка доступа.";
                    return View("UserErrorPage");
                }
            }
            else
            {
                TempData.Remove("TokenBody");
                TempData.Remove("LayoutFio");
                return RedirectToAction("Authorize");
            }
        }

        public IActionResult DeleteReservationAdmin(string userId)
        {
            if (!TempData.ContainsKey("TokenBody"))
            {
                ViewData["ErrorTextMessage"] = "Невозможно получить информацию о токене";
                return View("UserErrorPage");
            }

            var responseCheckToken = PostRequest.ExecuteAsync<UserBasicInfo>(UsersManagerUrl + "/api/Users/CheckToken", TempData.Peek("TokenBody").ToString()).Result;
            if (responseCheckToken.IsSuccess)
            {
                if (responseCheckToken.Data.IsAdmin)
                {
                    var responseDeleteReservation = PostRequest.ExecuteAsync<bool>(RecordsUrl + "/api/Records/DeleteReservation", Guid.Parse(userId)).Result;
                    if (responseDeleteReservation.IsSuccess)
                    {
                            return View("AdminDeletedReservation");  
                    }
                    else
                    {
                        ViewData["ErrorTextMessage"] = "Невозможно удалить запись пользователя. Запись не найдена или сервер недоступен.";
                        return View("UserErrorPage");
                    }
                }
                else
                {
                    ViewData["ErrorTextMessage"] = "Невозможно подтвердить права администратора. Ошибка доступа.";
                    return View("UserErrorPage");
                }
            }
            else
            {
                TempData.Remove("TokenBody");
                TempData.Remove("LayoutFio");
                return RedirectToAction("Authorize");
            }
        }

        public IActionResult СhangeReservationAdmin(string userId, string doctorName, string doctorLastName, string doctorMiddleName, string reservationTime)
        {
            if (!TempData.ContainsKey("TokenBody"))
            {
                ViewData["ErrorTextMessage"] = "Невозможно получить информацию о токене";
                return View("UserErrorPage");
            }

            var responseCheckToken = PostRequest.ExecuteAsync<UserBasicInfo>(UsersManagerUrl + "/api/Users/CheckToken", TempData.Peek("TokenBody").ToString()).Result;
            if (responseCheckToken.IsSuccess)
            {
                if (responseCheckToken.Data.IsAdmin)
                {
                    var responseDoctor = GetRequest.ExecuteAsync<Doctor>(RecordsUrl + $"/api/Records/Doctors/{doctorName}/{doctorLastName}/{doctorMiddleName}").Result;
                    if (responseDoctor.IsSuccess)
                    {
                        var responseDeleteReservation = PostRequest.ExecuteAsync<bool>(RecordsUrl + "/api/Records/DeleteReservation", Guid.Parse(userId)).Result;
                        if (!responseDeleteReservation.IsSuccess)
                        {
                            ViewData["ErrorTextMessage"] = "Невозможно удалить предыдущую запись пользователя. Запись не найдена или сервер недоступен.";
                            return View("UserErrorPage");
                        }

                        ReservationInfo reservationInfo = new ReservationInfo(Guid.Parse(userId), responseDoctor.Data.Id, DateTime.Parse(reservationTime));
                        var response = PostRequest.ExecuteAsync<(Reservation, bool)>(RecordsUrl + "/api/Records/AddReservation", reservationInfo).Result;
                        if (response.Data.Item2 == false)
                        {
                            if (response.Data.Item1 != null)
                                return View();
                            else
                            {
                                ViewData["ErrorTextMessage"] = "Невозможно добавить информацию о записе ко врачу. У пользователя уже есть активная запись.";
                                return View("UserErrorPage");
                            }
                        }
                        else
                        {
                            ViewData["ErrorTextMessage"] = "Невозможно добавить информацию о записе ко врачу. Ошибка сервера. Доктор не найден";
                            return View("UserErrorPage");
                        }
                    }
                    else
                    {
                        ViewData["ErrorTextMessage"] = "Невозможно найти доктора с таким ФИО.";
                        return View("UserErrorPage");
                    }
                }
                else
                {
                    ViewData["ErrorTextMessage"] = "Невозможно подтвердить права администратора. Ошибка доступа.";
                    return View("UserErrorPage");
                }
            }
            else
            {
                TempData.Remove("TokenBody");
                TempData.Remove("LayoutFio");
                return RedirectToAction("Authorize");
            }
        }
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult AdminDialogs()
        {
            if (!TempData.ContainsKey("TokenBody"))
            {
                ViewData["ErrorTextMessage"] = "Невозможно получить информацию о токене";
                return View("UserErrorPage");
            }

            var responseCheckToken = PostRequest.ExecuteAsync<UserBasicInfo>(UsersManagerUrl + "/api/Users/CheckToken", TempData.Peek("TokenBody")).Result;
            if (!responseCheckToken.IsSuccess)
            {
                TempData.Remove("TokenBody");
                TempData.Remove("LayoutFio");
                return RedirectToAction("Authorize");
            }

            if (!responseCheckToken.Data.IsAdmin)
            {
                ViewData["ErrorTextMessage"] = "Недостаточно прав для данной операции.";
                return View("UserErrorPage");
            }

            var guidsResponse = GetRequest.ExecuteAsync<List<Guid>>($"{CommunicationUrl}/api/Communication/ChattingUsersGuids").Result;
            if (!guidsResponse.IsSuccess)
            {
                ViewData["ErrorTextMessage"] = "Невозможно установить соедиенение с сервисом сообщений. Попробуйте позже.";
                return View("UserErrorPage");
            }
            guidsResponse.Data.Remove(responseCheckToken.Data.Id);

            var dialogsResponse = PostRequest.ExecuteAsync<List<UserBasicInfo>>($"{UsersManagerUrl}/api/Users/UsersInfo", guidsResponse.Data).Result;
            if (!dialogsResponse.IsSuccess)
            {
                ViewData["ErrorTextMessage"] = "Невозможно установить соедиенение с сервисом управления пользователями. Попробуйте позже.";
                return View("UserErrorPage");
            }

            return View(dialogsResponse.Data);
        }

        public IActionResult AdminDialog(string userId)
        {
            if (!TempData.ContainsKey("TokenBody"))
            {
                ViewData["ErrorTextMessage"] = "Невозможно получить информацию о токене";
                return View("UserErrorPage");
            }

            var responseCheckToken = PostRequest.ExecuteAsync<UserBasicInfo>(UsersManagerUrl + "/api/Users/CheckToken", TempData.Peek("TokenBody")).Result;
            if (!responseCheckToken.IsSuccess)
            {
                TempData.Remove("TokenBody");
                TempData.Remove("LayoutFio");
                return RedirectToAction("Authorize");
            }

            if (!responseCheckToken.Data.IsAdmin)
            {
                ViewData["ErrorTextMessage"] = "Недостаточно прав для данной операции.";
                return View("UserErrorPage");
            }


            var dialogsResponse = GetRequest.ExecuteAsync<(List<(Message message, bool isAdminAnswer)> messages, bool isServerError)>(CommunicationUrl 
                + $"/api/Communication/AdminMessage/{responseCheckToken.Data.Id}/{userId}").Result;

            if (!dialogsResponse.IsSuccess)
            {
                ViewData["ErrorTextMessage"] = "Ошибка сервера. Попробуйте позже.";
                return View("UserErrorPage");
            }

            if (dialogsResponse.Data.isServerError)
            {
                ViewData["ErrorTextMessage"] = "Невозможно просмотреть информацию о ваших сообщениях. Ошибка сервера.";
                return View("UserErrorPage");
            }

            return View(dialogsResponse.Data.messages);
        }

        [HttpPost]
        public IActionResult AddAdminMessage(string messageText, string hostName, string userId)
        {
            if (!TempData.ContainsKey("TokenBody"))
            {
                return NotFound(new { error = "Ошибка авторизации." });
            }

            var responseCheckToken = PostRequest.ExecuteAsync<UserBasicInfo>(UsersManagerUrl + "/api/Users/CheckToken", TempData.Peek("TokenBody")).Result;
            if (!responseCheckToken.IsSuccess)
            {
                TempData.Remove("TokenBody");
                TempData.Remove("LayoutFio");
                return NotFound(new { error = "Ошибка авторизации." });
            }

            if (!responseCheckToken.Data.IsAdmin)
            {
                return NotFound(new { error = "Недостаточно прав для выполнения данной операции." });
            }

            var message = new Message(responseCheckToken.Data.Id, Guid.Parse(userId), messageText);
            var response = PostRequest.ExecuteAsync<Message>(CommunicationUrl + "/api/Communication/AddMessage", message).Result;

            if (response.IsSuccess)
            {
                return Ok(new { uri = hostName + "/App/MessageAddSuccess" });
            }
            else
            {
                return Ok(new { uri = hostName + "/App/MessageAddFail" });
            }
               
        }
    }
}
