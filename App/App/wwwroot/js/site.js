﻿function sendRegistrationInputs(regForm) {
    var _email = regForm.Email.value;
    var _password = regForm.Password.value;

    var _name = regForm.Name.value;
    var _lastName = regForm.LastName.value;
    var _middleName = regForm.MiddleName.value;

    var _mobilePhone = regForm.MobilePhone.value;
    var _birthDate = regForm.BirthDate.value;

    var shaObj = new jsSHA("SHA-256", "TEXT");
    var _salt = "HowToBeASuperHero";
    shaObj.update(_password + _salt);
    var _hash = shaObj.getHash("B64");
    var errorShown = false;

    $.post("RegisterUser",
        {
            email: _email,
            passwordHash: _hash,

            name: _name,
            middleName: _middleName,
            lastName: _lastName,

            mobilePhone: _mobilePhone,
            birthDate: _birthDate,

            hostname: window.location.protocol + '//' + window.location.host
        })
        .done(function (resp) {
            window.location.href = resp.uri;
        })
        .fail(function () {
            alert("Request to server failed");
            errorShown = true;
        })
        .error(function () {
            if (errorShown === false)
                alert("Request to server failed");
        });
}

function sendAuthorizationInfo(authorizeForm) {
    var _email = authorizeForm.Email.value;
    var _password = authorizeForm.Password.value;

    var shaObj = new jsSHA("SHA-256", "TEXT");
    var _salt = "HowToBeASuperHero";
    shaObj.update(_password + _salt);
    var _hash = shaObj.getHash("B64");
    var errorShown = false;

    $.post("AuthorizeUser",
        {
            email: _email,
            passwordHash: _hash,

            hostname: window.location.protocol + '//' + window.location.host
        })
        .done(function (resp) {
            window.location.href = resp.uri;
        })
        .fail(function () {
            alert("Request to server failed");
            errorShown = true;
        })
        .error(function () {
            if (errorShown === false)
                alert("Request to server failed");
        });
}

function doctorsTableClick(name, lastName, middleName, isAuthorized) {
    if (isAuthorized == "False")
    {
        var wantAuthorize = confirm("Вы не авторизованы. Без авторизации записаться невозможно. Хотите перейти к авторизации?");
        if (wantAuthorize)
        {
            window.location.replace(window.location.protocol + '//' + window.location.host + '/App/Authorize');
        }
    }
    else
        window.location.replace(window.location.protocol + '//' + window.location.host + '/App/CalendarReservation?name=' + name + '&lastName=' + lastName + '&middleName=' + middleName);
}

function findSchedule(findSchedule, doctorId) {
    var _calendarDate = findSchedule.calendarDate.value;
    var _doctorId = doctorId;
    location = "/App/DoctorSchedule?currentDate=" + _calendarDate + "&doctorId=" + _doctorId
}

function scheduleTableClick(time, doctorId, isAuthorized) {
    if (isAuthorized == "False") {
        var wantAuthorize = confirm("Вы не авторизованы. Без авторизации записаться невозможно. Хотите перейти к авторизации?");
        if (wantAuthorize) {
            window.location.replace(window.location.protocol + '//' + window.location.host + '/App/Authorize');
        }
    }
    else
    {
        var wantReserve = confirm("Вы записывайтесь на " + time + ". Вы уверены?");
        if (wantReserve)
            window.location.replace(window.location.protocol + '//' + window.location.host + '/App/AddUserReservation?time=' + time + '&doctorId=' + doctorId);
    }
}

function sendMessage(messageForm) {
    var _messageText = messageForm.messageText.value;
    var errorShown = false;
    $.post("AddMessage",
        {
            messageText: _messageText,
            hostname: window.location.protocol + '//' + window.location.host
        })
        .done(function (resp) {
            window.location.href = resp.uri;
        })
        .fail(function (resp) {
            if ((typeof resp['responseJSON'] === "undefined") || (typeof resp.responseJSON['error'] === "undefined")) {
                alert("Не удалось получить ответ от сервера");
            } else {
                alert(resp.responseJSON.error);
            }
            errorShown = true;
        })
        .error(function (resp) {
            if (errorShown === false) {
                if ((typeof resp['responseJSON'] === "undefined") || (typeof resp.responseJSON['error'] === "undefined")) {
                    alert("Не удалось получить ответ от сервера");
                } else {
                    alert(resp.responseJSON.error);
                }
            }
        });
}

function sendAdminMessage(messageForm, adresseeId) {
    var _messageText = messageForm.messageText.value;
    var errorShown = false;
    $.post("AddAdminMessage",
        {
            messageText: _messageText,
            hostname: window.location.protocol + '//' + window.location.host,
            userId: adresseeId
        })
        .done(function (resp) {
            window.location.href = resp.uri;
        })
        .fail(function (resp) {
            if ((typeof resp['responseJSON'] === "undefined") || (typeof resp.responseJSON['error'] === "undefined")) {
                alert("Не удалось получить ответ от сервера");
            } else {
                alert(resp.responseJSON.error);
            }
            errorShown = true;
        })
        .error(function (resp) {
            if (errorShown === false) {
                if ((typeof resp['responseJSON'] === "undefined") || (typeof resp.responseJSON['error'] === "undefined")) {
                    alert("Не удалось получить ответ от сервера");
                } else {
                    alert(resp.responseJSON.error);
                }
            }
        });
}

function selectUsersReservationsAdmin(chooseUser) {

    var _userId = chooseUser.choose.value;
    var errorShown = false;
    window.location.replace(window.location.protocol + '//' + window.location.host + '/App/UserReservationAdmin?userId=' + _userId);
}

function changeReservationAdmin(userId, tableInput)
{
    var doctorName = tableInput.doctorName.value;
    var doctorLastName = tableInput.doctorLastName.value;
    var doctorMiddleName = tableInput.doctorMiddleName.value;
    var reservationTime = tableInput.reservationTime.value;

    window.location.replace(window.location.protocol + '//' + window.location.host + '/App/СhangeReservationAdmin?userId=' + userId +
        '&doctorName=' + doctorName + '&doctorLastName=' + doctorLastName + '&doctorMiddleName=' + doctorMiddleName + '&reservationTime=' + reservationTime);
}

function deleteReservationAdmin(userId) {
    window.location.replace(window.location.protocol + '//' + window.location.host + '/App/deleteReservationAdmin?userId=' + userId);
}