﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Records.Models;

namespace Records.Converters
{
    public class DayToStringConverter
    {
        public static string Convert (Days d)
        {
            string result = "";
            switch (d)
            {
                case Days.Monday:
                    result = "Понедельник";
                    break;
                case Days.Tuesday:
                    result = "Вторник";
                    break;
                case Days.Wednesday:
                    result = "Среда";
                    break;
                case Days.Thursday:
                    result = "Четверг";
                    break;
                case Days.Friday:
                    result = "Пятница";
                    break;
                case Days.Saturday:
                    result = "Суббота";
                    break;
                case Days.Sunday:
                    result = "Воскресенье";
                    break;
                default:
                    result = "Неизвестно";
                    break;
            }
            return result;
        }
    }
}
