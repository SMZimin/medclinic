﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Records.Models;

namespace Records.Converters
{
    public class SpecialityToStringConverter
    {
        public static string Convert(Specialities spec)
        {
            string result = "";
            switch (spec)
            {
                case Specialities.Orthopedist:
                    result = "Ортопед";
                    break;
                case Specialities.Therapist:
                    result = "Терапевт";
                    break;
                case Specialities.Surgeon:
                    result = "Хирург";
                    break;
                case Specialities.Orthodontist:
                    result = "Ортодонт";
                    break;
                case Specialities.Implantologist:
                    result = "Имплантолог";
                    break;
                case Specialities.Hygienist:
                    result = "Гигиенист";
                    break;
                default:
                    result = "Неизвестно";
                    break;
            }
            return result;
        }
    }
}
