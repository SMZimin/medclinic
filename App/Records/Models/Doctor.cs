﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Records.Models
{
    public class Doctor
    {
        public Guid Id { get; set; }

        public virtual List<DailySchedule> Schedule { get; set; } // время работы

        // Другие поля доктора: ФИО и тд
        public string Name { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public Specialities Speciality { get; set; }

        public Doctor()
        {

        }
    }
}
