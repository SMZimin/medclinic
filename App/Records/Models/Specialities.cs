﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Records.Models
{
    public enum Specialities
    {
        Orthopedist = 0,
        Therapist = 1,
        Surgeon = 2,
        Orthodontist = 3,
        Implantologist = 4,
        Hygienist = 5
    }
}
