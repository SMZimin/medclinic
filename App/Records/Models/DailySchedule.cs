﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Records.Models
{
    public class DailySchedule // Расписание на один из семи дней недели (должно быть по 7 на каждого доткора)
    {
        public Guid Id { get; set; }

        public Guid DoctorId { get; set; }
        //public virtual Doctor Doctor { get; set; }

        public Days Day { get; set; }

        public DateTime? BeginingTime { get; set; }

        public DateTime? EndTime { get; set; }

        public bool IsWorkingDay
        {
            get
            {
                return BeginingTime.HasValue && EndTime.HasValue;
            }
        }
    }
}
