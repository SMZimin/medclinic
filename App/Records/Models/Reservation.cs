﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Records.Models
{
    public class Reservation
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public Guid DoctorId { get; set; }
        public virtual Doctor Doctor { get; set; }

        public DateTime ReservationDateTime { get; set; }

        public Reservation (Guid userId, Guid doctorId, Doctor doctor, DateTime reservationDateTime)
        {
            this.Id = Guid.NewGuid();
            this.UserId = userId;

            this.DoctorId = doctorId;
            this.Doctor = doctor;

            this.ReservationDateTime = reservationDateTime;
        }
        public Reservation()
        {

        }
    }
}
