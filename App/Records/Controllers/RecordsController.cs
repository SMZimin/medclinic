﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DataTransferObjects;
using Records.Data;
using Records.Models;
using Records.Converters;

namespace Records.Controllers
{
    [Route("api/[controller]")]
    public class RecordsController : Controller
    {
        RecordsContext db;

        public RecordsController(RecordsContext context)
        {
            this.db = context;
        }

        [HttpGet]
        [Route("Doctors")]
        public List<Doctor> Get()
        {
            return db.Doctors.Include(d => d.Schedule).ToList();
        }

        [HttpGet]
        [Route("Doctors/{name}/{lastName}/{middleName}")]
        public Doctor Get(string name, string lastName, string middleName)
        {  
            return db.Doctors.Include(d => d.Schedule).FirstOrDefault(d => d.Name == name && d.LastName == lastName && d.MiddleName == middleName);
        }

        [HttpGet]
        [Route("Doctors/{currentDateString}/{doctorIdString}")]
        public List<(DateTime,bool)> Get(string currentDateString, string doctorIdString)
        {
            IQueryable<Reservation> reservations = db.Reservations.Where(r => r.DoctorId == Guid.Parse(doctorIdString) && r.ReservationDateTime.ToShortDateString() == DateTime.Parse(currentDateString).ToShortDateString());
            List<DateTime> reservedHours = reservations.Select(r => r.ReservationDateTime).ToList();

            // Выбираем расписание доктора на данный день недели, чтобы далее отсечь нерабочие часы по графику
            int numDay = (int)DateTime.Parse(currentDateString).DayOfWeek - 1 < 0 ? 0 : (int)DateTime.Parse(currentDateString).DayOfWeek - 1;
            DailySchedule schedule = db.DailySchedules.FirstOrDefault(s => s.DoctorId == Guid.Parse(doctorIdString) && (int)s.Day == numDay);
            
            List<(DateTime, bool)> timeSet = new List<(DateTime, bool)>();
            DateTime currentDateStart = DateTime.Parse(currentDateString).AddHours(8);

            for (int i = 0; i < 13; i++, currentDateStart = currentDateStart.AddHours(1))
            {
                // Время занято, если есть запись или вне рабочего времени
                bool reservedFlag = reservedHours.Contains(currentDateStart) || !schedule.IsWorkingDay
                    || currentDateStart.Hour < schedule.BeginingTime.Value.Hour || currentDateStart.Hour > schedule.EndTime.Value.Hour;

                (DateTime, bool) addingTime = (currentDateStart, reservedFlag);
                timeSet.Add(addingTime);
            }
            return timeSet;
        }

        // GET api/values/5
        [HttpPost]
        [Route("UserReservation")]
        public (Reservation,bool) UserReservation([FromBody] Guid id)
        {
            bool isServerError = false;
            Reservation reservation = db.Reservations.Include(r => r.Doctor).FirstOrDefault(r => r.UserId == id && r.ReservationDateTime > DateTime.Now);
            if (reservation != null)
            {
                return (reservation, isServerError);
            }
            return (null, isServerError = true);
        }

        // POST api/values
        [HttpPost]
        [Route("AddReservation")]
        public (Reservation,bool) AddReservation([FromBody]ReservationInfo reservationInfo)
        {
            bool isServerError = false;
            Doctor doctor = db.Doctors.FirstOrDefault(d => d.Id == reservationInfo.DoctorId);
            if (doctor != null)
            {
                if (!db.Reservations.Any(r => r.UserId == reservationInfo.UserId && r.ReservationDateTime > DateTime.Now))
                {
                    Reservation reservation = new Reservation(reservationInfo.UserId, reservationInfo.DoctorId, doctor, reservationInfo.Time);
                    db.Reservations.Add(reservation);
                    db.SaveChanges();
                    return (reservation, isServerError);
                }
                else
                {
                    return (null, isServerError);
                }
            }
            return (null, isServerError = true);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpPost]
        [Route("DeleteReservation")]
        public bool DeleteReservation([FromBody] Guid userId)
        {
            Reservation destReservation = db.Reservations.FirstOrDefault(r => r.UserId == userId && r.ReservationDateTime > DateTime.Now);
            if (destReservation != null)
            {
                db.Reservations.Remove(destReservation);
                db.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
