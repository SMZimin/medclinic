﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Records.Models;


namespace Records.Data
{
    public class RecordsContext: DbContext
    {
        public RecordsContext(DbContextOptions<RecordsContext> options) : base(options)
        {
        }

        public DbSet<DailySchedule> DailySchedules { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Reservation> Reservations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DailySchedule>().ToTable("DailySchedules");
            modelBuilder.Entity<Doctor>().ToTable("Doctors");
            modelBuilder.Entity<Reservation>().ToTable("Reservations");
        }
    }
}
