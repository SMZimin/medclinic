﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Records.Models;

namespace Records.Data
{
    public class DBInitializer
    {
        public static void Initialize(RecordsContext context)
        {
            context.Database.EnsureCreated();

            if (context.Doctors.Any() && context.DailySchedules.Any())
            {
                return;   // DB has been seeded
            }

            var doctors = new Doctor[]
            {
                new Doctor { Name = "Виталий", LastName = "Сидоренко", MiddleName = "Игоревич", Speciality = Specialities.Orthopedist },
                new Doctor { Name = "Нина", LastName = "Проценко", MiddleName = "Владимировна", Speciality = Specialities.Therapist },
                new Doctor { Name = "Игорь", LastName = "Проценко", MiddleName = "Викторович", Speciality = Specialities.Surgeon },
                new Doctor { Name = "Наталья", LastName = "Моисеева", MiddleName = "Васильевна", Speciality = Specialities.Orthodontist },
                new Doctor { Name = "Алексей", LastName = "Иващенко", MiddleName = "Иванович", Speciality = Specialities.Implantologist },
            };
            foreach (Doctor d in doctors)
            {
                context.Doctors.Add(d);
            }
            context.SaveChanges();

            Doctor currentDoctor = context.Doctors.FirstOrDefault(x => x.Name == "Виталий");
            var dailyschedules = new DailySchedule[]
            {
                new DailySchedule {DoctorId = currentDoctor.Id, Day = Days.Monday, BeginingTime = DateTime.Parse("10:00"), EndTime = DateTime.Parse("19:00")},
                new DailySchedule {DoctorId = currentDoctor.Id, Day = Days.Tuesday, BeginingTime = DateTime.Parse("12:00"), EndTime = DateTime.Parse("16:00")},
                new DailySchedule {DoctorId = currentDoctor.Id, Day = Days.Wednesday, BeginingTime = DateTime.Parse("10:00"), EndTime = DateTime.Parse("19:00")},
                new DailySchedule {DoctorId = currentDoctor.Id, Day = Days.Thursday, BeginingTime = DateTime.Parse("12:00"), EndTime = DateTime.Parse("19:00")},
                new DailySchedule {DoctorId = currentDoctor.Id, Day = Days.Friday, BeginingTime = DateTime.Parse("10:00"), EndTime = DateTime.Parse("14:00")},
                new DailySchedule {DoctorId = currentDoctor.Id, Day = Days.Saturday, BeginingTime = DateTime.Parse("10:00"), EndTime = DateTime.Parse("19:00")},
                new DailySchedule {DoctorId = currentDoctor.Id, Day = Days.Sunday},
            };
            foreach (DailySchedule d in dailyschedules)
            {
                context.DailySchedules.Add(d);
            }

            context.SaveChanges();

            currentDoctor = context.Doctors.FirstOrDefault(x => x.Name == "Алексей");
            dailyschedules = new DailySchedule[]
            {
                new DailySchedule {DoctorId = currentDoctor.Id, Day = Days.Monday, BeginingTime = DateTime.Parse("15:00"), EndTime = DateTime.Parse("20:00")},
                new DailySchedule {DoctorId = currentDoctor.Id, Day = Days.Tuesday, BeginingTime = DateTime.Parse("15:00"), EndTime = DateTime.Parse("20:00")},
                new DailySchedule {DoctorId = currentDoctor.Id, Day = Days.Wednesday, BeginingTime = DateTime.Parse("16:00"), EndTime = DateTime.Parse("19:00")},
                new DailySchedule {DoctorId = currentDoctor.Id, Day = Days.Thursday, BeginingTime = DateTime.Parse("15:00"), EndTime = DateTime.Parse("20:00")},
                new DailySchedule {DoctorId = currentDoctor.Id, Day = Days.Friday},
                new DailySchedule {DoctorId = currentDoctor.Id, Day = Days.Saturday, BeginingTime = DateTime.Parse("14:00"), EndTime = DateTime.Parse("17:00")},
                new DailySchedule {DoctorId = currentDoctor.Id, Day = Days.Sunday},
            };
            foreach (DailySchedule d in dailyschedules)
            {
                context.DailySchedules.Add(d);
            }


            context.SaveChanges();
        }
    }
}
