﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UsersManager.Data;
using UsersManager.Models;
using DataTransferObjects;

namespace UsersManager.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        UsersManagerContext db;

        public UsersController(UsersManagerContext context)
        {
            this.db = context;
        }


        [HttpGet]
        [Route("ListUsers")]
        public List<UserBasicInfo> ListUsers()
        {
            List<UserBasicInfo> userBasicInfo = new List<UserBasicInfo>();
            foreach (var user in db.Users)
            {
                userBasicInfo.Add(new UserBasicInfo(user.Id, user.Name, user.MiddleName, user.LastName, user.IsAdmin));
            }
            return userBasicInfo;
        }

        [HttpGet]
        [Route("FindAdmin")]
        public UserBasicInfo FindAdmin()
        {
            User user = db.Users.FirstOrDefault(u => u.AdminFlg == 1);
            UserBasicInfo userBasicInfo = new UserBasicInfo(user.Id, user.Name, user.MiddleName, user.LastName, true);
            return userBasicInfo;
        }
     
        // POST api/values
        [HttpPost]
        [Route("RegisterUser")]
        public UserBasicInfo RegisterUser([FromBody] UserFullInfo newUserFullInfo)
        {
            User newUser = new User(newUserFullInfo.Name, newUserFullInfo.MiddleName, newUserFullInfo.LastName,
                newUserFullInfo.MobilePhone, newUserFullInfo.BirthDate, newUserFullInfo.Email, newUserFullInfo.PasswordHash);

            if (db.Users.Any(x => x.Email == newUser.Email || x.MobilePhone == newUser.MobilePhone))
                return null;

            db.Users.Add(newUser);
            db.SaveChanges();

            UserBasicInfo userInfo = new UserBasicInfo(newUser.Id, newUser.Name, newUser.MiddleName, newUser.LastName, false);

            return userInfo;
        }

        [HttpPost]
        [Route("VerifyUser")]
        public UserAuthResponse VerifyUser([FromBody] UserAuthInfo user)
        {
            User possibleUser = db.Users.FirstOrDefault(x => x.Email == user.Email && x.PasswordHash == user.PasswordHash);
            if (possibleUser != null)
            {
                
                UserBasicInfo userBasicInfo = new UserBasicInfo(possibleUser.Id, possibleUser.Name, possibleUser.MiddleName, possibleUser.LastName, possibleUser.IsAdmin);

                Token tk = new Token(possibleUser.Id, possibleUser);

                UserAuthResponse userAuthResponse = new UserAuthResponse(tk.Body, userBasicInfo);
                db.Tokens.Add(tk);
                db.SaveChanges();

                return userAuthResponse;
            }
            
            return null;
        }

        [HttpPost]
        [Route("CheckToken")]
        public UserBasicInfo CheckToken([FromBody] string body)
        {
            Token tempToken = db.Tokens.Include(t => t.User).FirstOrDefault(x => x.Body == body && x.ExpiresAt > DateTime.Now);

            if (tempToken != null)
            {
                UserBasicInfo userInfo = new UserBasicInfo(tempToken.User.Id, tempToken.User.Name,
                    tempToken.User.MiddleName, tempToken.User.LastName, tempToken.User.IsAdmin);
                return userInfo;
            }

            return null;
        }

        [HttpPost]
        [Route("UsersInfo")]
        public List<UserBasicInfo> GetUsersInfo([FromBody]List<Guid> usersGuids)
        {
            var res = new List<UserBasicInfo>();
            foreach (var id in usersGuids)
            {
                var user = db.Users.First(u => u.Id == id);
                res.Add(new UserBasicInfo(user.Id, user.Name, user.MiddleName, user.LastName, user.IsAdmin));
            }
            return res;
        }


        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
