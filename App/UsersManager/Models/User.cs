﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace UsersManager.Models
{
    public class User
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public string MobilePhone { get; set; }
        public DateTime BirthDate { get; set; }

        public string Email { get; set; }
        public string PasswordHash { get; set; }

        public int AdminFlg { get; set; }

        public virtual List<Token> Tokens { get; set; }

        public User(string name, string middleName, string lastName, string mobilePhone, DateTime birthDate, 
            string email, string passwordHash)
        {
            this.Name = name;
            this.MiddleName = middleName;
            this.LastName = lastName;

            this.MobilePhone = mobilePhone;
            this.BirthDate = birthDate;

            this.Email = email;
            this.PasswordHash = passwordHash;

            AdminFlg = 0;
            this.Id = Guid.NewGuid();
        }

        public User()
        {

        }

        [NotMapped]
        public bool IsAdmin => AdminFlg == 1 ? true : false;
    }
}
