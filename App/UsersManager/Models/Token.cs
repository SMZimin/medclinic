﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsersManager.Models
{
    public class Token
    {
        public Guid Id { get; set; }
        public string Body { get; set; }
        public DateTime ExpiresAt { get; set; }

        public Guid UserId { get; set; }
        public virtual User User { get; set; }

        public Token(Guid UserId, User User)
        {
            this.UserId = UserId;
            this.User = User;

            Id = Guid.NewGuid();
            this.Body = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            this.ExpiresAt = DateTime.Now.AddMinutes(3);
        }
        public Token()
        {

        }
    }
}
