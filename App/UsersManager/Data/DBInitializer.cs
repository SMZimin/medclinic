﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UsersManager.Models;

namespace UsersManager.Data
{
    public class DBInitializer
    {
        public static void Initialize(UsersManagerContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Users.Any())
            {
                return;   // DB has been seeded
            }

            var users = new User[]
            {
	            new User { Name = "Сергей", MiddleName = "Михайлович", LastName = "Зимин", MobilePhone = "+79777079343", BirthDate = DateTime.Parse("1995-07-05"), AdminFlg = 1,  Email = "zexp@yandex.ru",PasswordHash = "QXKoohfBYsh7kAZ9POlk3qzla1o9wrU85SytpsIVp5s=" },
	            new User { Name = "Денис", MiddleName = "Сергеевич", LastName = "Бабарыкин", MobilePhone = "+71234567891", BirthDate = DateTime.Parse("1994-08-06"), AdminFlg = 0,  Email = "ya.babarykin@yandex.ru",PasswordHash = "54321" },
	            new User { Name ="София", MiddleName = "Алексеевна", LastName = "Суровцева", MobilePhone = "+72345678910", BirthDate = DateTime.Parse("1997-10-08"), AdminFlg = 0, Email = "sofia@yandex.ru",PasswordHash = "sofia" },
	            new User { Name = "Иван", MiddleName = "Батькович", LastName = "Антонов", MobilePhone = "+73456789101", BirthDate = DateTime.Parse("1994-12-13"), AdminFlg = 0, Email = "ivan@yandex.ru",PasswordHash = "ivan" },
	            new User { Name = "Максим", MiddleName = "Батькович", LastName = "Земцов", MobilePhone = "+74567891011", BirthDate = DateTime.Parse("1994-08-22"), AdminFlg = 0, Email = "maksik@yandex.ru",PasswordHash = "maksik" },
	            new User { Name = "Вавыка", MiddleName = "Батькович", LastName = "Шарапов", MobilePhone = "+75678910111", BirthDate = DateTime.Parse("1994-11-15"), AdminFlg = 0, Email = "vavika@yandex.ru",PasswordHash = "vavika" },
	            new User { Name = "Мавыка", MiddleName = "Батьковна", LastName = "Еремычева", MobilePhone = "+76789101112", BirthDate = DateTime.Parse("1995-09-17"), AdminFlg = 0, Email = "mavika@yandex.ru",PasswordHash = "mavika" },
	            new User { Name = "Руслан", MiddleName = "Батькович", LastName = "Ахтямов", MobilePhone = "+77891011121", BirthDate = DateTime.Parse("1994-10-02"), AdminFlg = 0, Email = "ruslan@yandex.ru",PasswordHash = "ruslan" },
	            new User { Name = "Юлия", MiddleName = "Батьковна", LastName = "Гаврилова", MobilePhone = "+78910111213", BirthDate = DateTime.Parse("1994-03-07"), AdminFlg = 0, Email = "julia@yandex.ru",PasswordHash = "julia" }
            };
            foreach (User s in users)
            {
                context.Users.Add(s);
            }

            context.SaveChanges();
        }
    }
}
