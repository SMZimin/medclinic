﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Communication.Models
{
    public class Message
    {
        public Guid Id { get; set; }

        public Guid FromUserId { get; set; }
        public Guid ToUserId { get; set; }
        public DateTime Date { get; set; }
        public string Text { get; set; }

        public Message (Guid fromUserId, Guid toUserId, string text)
        {
            this.FromUserId = fromUserId;
            this.ToUserId = toUserId;
            this.Text = text;

            this.Date = DateTime.Now;
        }
        public Message()
        {

        }
    }
}
