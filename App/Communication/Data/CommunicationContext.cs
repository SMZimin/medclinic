﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Communication.Models;
using Microsoft.EntityFrameworkCore;

namespace Communication.Data
{
    public class CommunicationContext: DbContext
    {
        public CommunicationContext(DbContextOptions<CommunicationContext> options) : base(options)
        {
        }

        public DbSet<Message> Messages { get; set; }
    }
}
