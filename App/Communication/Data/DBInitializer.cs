﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Communication.Models;

namespace Communication.Data
{
    public class DBInitializer
    {
        public static void Initialize(CommunicationContext context)
        {
            context.Database.EnsureCreated();
        }
    }
}
