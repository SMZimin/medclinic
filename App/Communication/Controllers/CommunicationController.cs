﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Communication.Data;
using Communication.Models;

namespace Communication.Controllers
{
    [Route("api/[controller]")]
    public class CommunicationController : Controller
    {
        CommunicationContext db;

        public CommunicationController(CommunicationContext context)
        {
            this.db = context;
        }

        [HttpGet]
        [Route("ChattingUsersGuids")]
        public List<Guid> GetChattingUsersGuids()
        {
            return db.Messages
                .OrderByDescending(m => m.Date)
                .Select(m => m.FromUserId)
                .Distinct()
                .ToList();
        }

        // GET api/values
        [HttpGet]
        [Route("Message/{userId}")]
        public (List<(Message, bool)>, bool) Message(string userId)
        {
            bool isServerError = false;
            bool isAdminAnswer = false;
            List<Message> messagesFrom = db.Messages.Where(m => m.FromUserId == Guid.Parse(userId)).ToList();
            List<Message> messagesTo = db.Messages.Where(m => m.ToUserId == Guid.Parse(userId)).ToList();
            List<(Message, bool)> messages = new List<(Message, bool)>();

            foreach (var message in messagesFrom)
            {
                messages.Add((message, isAdminAnswer));
            }

            isAdminAnswer = true;
            foreach (var message in messagesTo)
            {
                messages.Add((message, isAdminAnswer));
            }

            messages = messages.OrderBy(o => o.Item1.Date).ToList();
            return (messages, isServerError);
        }

        [HttpGet]
        [Route("AdminMessage/{adminId}/{userId}")]
        public (List<(Message message, bool isAdminAnswer)> messages, bool isServerError) GetAdminMessage(string adminId, string userId)
        {
            var messagesFrom = db.Messages.Where(m => m.FromUserId == Guid.Parse(adminId) && m.ToUserId == Guid.Parse(userId)).ToList();
            var messagesTo = db.Messages.Where(m => m.FromUserId == Guid.Parse(userId) && m.ToUserId == Guid.Parse(adminId)).ToList();
            var messages = new List<(Message message, bool isAdminAnswer)>();

            foreach (var message in messagesFrom)
            {
                messages.Add((message, false));
            }

            foreach (var message in messagesTo)
            {
                messages.Add((message, true));
            }

            return (messages.OrderBy(o => o.message.Date).ToList(), false);
        }

        // POST api/values
        [HttpPost]
        [Route("AddMessage")]
        public Message AddMessage([FromBody] Message message)
        {
            db.Messages.Add(message);
            db.SaveChanges();
            return message;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
