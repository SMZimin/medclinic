﻿using HttpRequests.DataFormats;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace HttpRequests
{
    public class PostRequest
    {
        /// <summary>Async POST request with body of "x-www-form-urlencoded" format</summary>
        /// <param name="url">String with url for POST request</param>
        /// <param name="postRequestBody">Body of POST request that will be serialized to "application/x-www-form-urlencoded" media type</param>
        /// <param name="responseDataType">In what format requested api responds the data. Json by default</param>
        /// <returns>Wrapped object of given generic type with response data</returns>
        public static async Task<RestResponse<T>> ExecuteAsync<T>(string url, IEnumerable<KeyValuePair<string, string>> postRequestBody, 
            ResponseDataFormat responseDataFormat = ResponseDataFormat.Json)
        {
            var result = new RestResponse<T>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.PostAsync(url, new FormUrlEncodedContent(postRequestBody));
                    result.IsSuccessStatusCode = response.IsSuccessStatusCode;
                    result.StatusCode = response.StatusCode;
                    if (result.IsSuccessStatusCode)
                    {
                        response.Content.Headers.ContentType.MediaType = "application/json";
                        result.Data = JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
                    }
                }
            }
            catch (Exception e)
            {
                result.Exception = e;
            }

            return result;
        }

        /// <summary>Async POST request with body of "x-www-form-urlencoded" format</summary>
        /// <param name="url">String with url for POST request</param>
        /// <param name="postRequestBody">Body of POST request that will be serialized to "application/x-www-form-urlencoded" media type</param>
        /// <param name="responseDataType">In what format requested api responds the data. Json by default</param>
        /// <returns>Wrapped object of given generic type with response data</returns>
        public static async Task<RestResponse> ExecuteAsync(string url, IEnumerable<KeyValuePair<string, string>> postRequestBody,
            ResponseDataFormat responseDataFormat = ResponseDataFormat.Json)
        {
            var result = new RestResponse();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.PostAsync(url, new FormUrlEncodedContent(postRequestBody));
                    result.IsSuccessStatusCode = response.IsSuccessStatusCode;
                    result.StatusCode = response.StatusCode;
                }
            }
            catch (Exception e)
            {
                result.Exception = e;
            }

            return result;
        }

        /// <summary>Async POST request with body that will be serialized to JSON type</summary>
        /// <param name="url">String with url for POST request</param>
        /// <param name="postRequestBody">Body of POST request that will be serialized to JSON</param>
        /// <param name="responseDataType">In what format requested api responds the data. Json by default</param>
        /// <returns>Wrapped object of given generic type with response data</returns>
        public static async Task<RestResponse<T>> ExecuteAsync<T>(string url, object postRequestBody, ResponseDataFormat responseDataFormat = ResponseDataFormat.Json)
        {
            var result = new RestResponse<T>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.PostAsync(url, new StringContent(JsonConvert.SerializeObject(postRequestBody), Encoding.UTF8, "application/json"));
                    result.IsSuccessStatusCode = response.IsSuccessStatusCode;
                    result.StatusCode = response.StatusCode;
                    if (result.IsSuccessStatusCode)
                    {
                        response.Content.Headers.ContentType.MediaType = "application/json";
                        result.Data = JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
                    }
                }
            }
            catch (Exception e)
            {
                result.Exception = e;
            }

            return result;
        }

        /// <summary>Async POST request with body that will be serialized to JSON type</summary>
        /// <param name="url">String with url for POST request</param>
        /// <param name="postRequestBody">Body of POST request that will be serialized to JSON</param>
        /// <param name="responseDataType">In what format requested api responds the data. Json by default</param>
        /// <returns>Wrapped object of given generic type with response data</returns>
        public static async Task<RestResponse> ExecuteAsync(string url, object postRequestBody, ResponseDataFormat responseDataFormat = ResponseDataFormat.Json)
        {
            var result = new RestResponse();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.PostAsync(url, new StringContent(JsonConvert.SerializeObject(postRequestBody), Encoding.UTF8, "application/json"));
                    result.IsSuccessStatusCode = response.IsSuccessStatusCode;
                    result.StatusCode = response.StatusCode;
                }
            }
            catch (Exception e)
            {
                result.Exception = e;
            }

            return result;
        }
    }
}
