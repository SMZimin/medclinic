﻿using HttpRequests.Converters;
using HttpRequests.DataFormats;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace HttpRequests
{
    public class GetRequest
    {
        /// <summary>Async GET request to the api</summary>
        /// <param name="url">String with url and parameters for GET request</param>
        /// <param name="responseDataType">In what format requested api responds the data. Json by default</param>
        ///<returns>Wrapped object of given generic type with response data</returns>
        public static async Task<RestResponse<T>> ExecuteAsync<T>(string url, ResponseDataFormat responseDataFormat = ResponseDataFormat.Json)
        {
            var result = new RestResponse<T>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync(url);
                    result.IsSuccessStatusCode = response.IsSuccessStatusCode;
                    result.StatusCode = response.StatusCode;
                    if (result.IsSuccessStatusCode)
                    {
                        response.Content.Headers.ContentType.MediaType = "application/json";
                        result.Data = JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
                    }
                }
            }
            catch (Exception e)
            {
                result.Exception = e;
            }

            return result;
        }

        /// <summary>Async GET request to the api</summary>
        /// <param name="url">String with url and parameters for GET request</param>
        /// <param name="responseDataType">In what format requested api responds the data. Json by default</param>
        ///<returns>Wrapped object of given generic type with response data</returns>
        public static async Task<RestResponse> ExecuteAsync(string url, ResponseDataFormat responseDataFormat = ResponseDataFormat.Json)
        {
            var result = new RestResponse();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync(url);
                    result.IsSuccessStatusCode = response.IsSuccessStatusCode;
                    result.StatusCode = response.StatusCode;
                }
            }
            catch (Exception e)
            {
                result.Exception = e;
            }

            return result;
        }

        /// <summary>Async GET request to the api that responses in JSON format</summary>
        /// <param name="url">String with url</param>
        /// <param name="requestParameters">Parameters for GET request as key-value pairs</param>
        /// <param name="responseDataType">In what format requested api responds the data. Json by default</param>
        /// <returns>Wrapped object of given generic type with response data</returns>
        public static async Task<RestResponse<T>> ExecuteAsync<T>(string url, IEnumerable<KeyValuePair<string, string>> requestParameters, 
            ResponseDataFormat responseDataFormat = ResponseDataFormat.Json)
        {
            var result = new RestResponse<T>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string urlWithParams = string.Format("{0}?{1}", url, KeyValueToStringConverter.Convert(requestParameters));
                    HttpResponseMessage response = await client.GetAsync(urlWithParams);
                    result.IsSuccessStatusCode = response.IsSuccessStatusCode;
                    result.StatusCode = response.StatusCode;
                    if (result.IsSuccessStatusCode)
                    {
                        response.Content.Headers.ContentType.MediaType = "application/json";
                        result.Data = JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
                    }
                }
            }
            catch (Exception e)
            {
                result.Exception = e;
            }

            return result;
        }

        /// <summary>Async GET request to the api that responses in JSON format</summary>
        /// <param name="url">String with url</param>
        /// <param name="requestParameters">Parameters for GET request as key-value pairs</param>
        /// <param name="responseDataType">In what format requested api responds the data. Json by default</param>
        /// <returns>Wrapped object of given generic type with response data</returns>
        public static async Task<RestResponse> ExecuteAsync(string url, IEnumerable<KeyValuePair<string, string>> requestParameters,
            ResponseDataFormat responseDataFormat = ResponseDataFormat.Json)
        {
            var result = new RestResponse();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string urlWithParams = string.Format("{0}?{1}", url, KeyValueToStringConverter.Convert(requestParameters));
                    HttpResponseMessage response = await client.GetAsync(urlWithParams);
                    result.IsSuccessStatusCode = response.IsSuccessStatusCode;
                    result.StatusCode = response.StatusCode;
                }
            }
            catch (Exception e)
            {
                result.Exception = e;
            }

            return result;
        }
    }
}
