﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace HttpRequests
{
    public class RestResponse
    {
        /// <summary>Exception if something went wrong. If all is ok - null.</summary>
        public Exception Exception { get; internal set; }

        /// <summary> Http status code of response </summary>
        public HttpStatusCode StatusCode { get; internal set; }

        /// <summary>Is status code of response was Success</summary>
        public bool IsSuccessStatusCode { get; internal set; }

        /// <summary>Main property to check before calling the <see cref="Data"/> property</summary>
        public bool IsSuccess => Exception == null && IsSuccessStatusCode;
    }

    public class RestResponse<T>: RestResponse
    {
        /// <summary>Data of response</summary>
        public T Data { get; internal set; } 
    }
}
