﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataTransferObjects
{
    public class UserBasicInfo
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public bool IsAdmin { get; set; }

        public UserBasicInfo(Guid guid, string name, string middlename, string surname, bool isAdmin)
        {
            Id = guid;

            Name = name;
            MiddleName = middlename;
            LastName = surname;
            IsAdmin = isAdmin;
        }
    }
}
