﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataTransferObjects
{
    public class ReservationInfo
    {
        public Guid UserId { get; set; }
        public Guid DoctorId { get; set; }
        public DateTime Time { get; set; }

        public ReservationInfo (Guid userId, Guid doctorId, DateTime time)
        {
            this.UserId = userId;
            this.DoctorId = doctorId;
            this.Time = time;
        }
    }
}
