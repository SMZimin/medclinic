﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataTransferObjects
{
    public class DoctorSheduleInfo
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public string Day { get; set; }
        public DateTime? BeginingTime { get; set; }
        public DateTime? EndTime { get; set; }

        public string Speciality { get; set; }

        public DoctorSheduleInfo (string name, string lastName, string middleName, string day, DateTime beginingTime, DateTime endTime, string speciality)
        {
            this.Name = name;
            this.LastName = lastName;
            this.MiddleName = middleName;

            this.Day = day;
            this.BeginingTime = beginingTime;
            this.EndTime = endTime;

            this.Speciality = speciality;
        }
    }
}
