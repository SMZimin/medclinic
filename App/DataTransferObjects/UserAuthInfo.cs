﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataTransferObjects
{
    public class UserAuthInfo
    {
        public string Email { get; set; }
        public string PasswordHash { get; set; }

        public UserAuthInfo(string email, string passwordHash)
        {
            this.Email = email;
            this.PasswordHash = passwordHash;
        }
    }
}
