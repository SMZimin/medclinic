﻿using System;

namespace DataTransferObjects
{
    public class UserFullInfo
    {
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string MobilePhone { get; set; }
        public DateTime BirthDate { get; set; }

        public UserFullInfo (string email ,string passwordHash, string name, 
            string lastName, string middleName, string mobilePhone, DateTime birthdate)
        {
            this.Email = email;
            this.PasswordHash = passwordHash;
            this.Name = name;
            this.LastName = lastName;
            this.MiddleName = middleName;
            this.MobilePhone = mobilePhone;
            this.BirthDate = birthdate;
        }
    }
}
