﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataTransferObjects
{
    public class UserAuthResponse
    {
        public string TokenBody { get; set; }

        public UserBasicInfo UserBasicInfo { get; set; }

        public UserAuthResponse(string tokenBody, UserBasicInfo userBasicInfo)
        {
            this.TokenBody = tokenBody;
            this.UserBasicInfo = userBasicInfo;
        }
    }
}
